# coding: utf-8
###
# Copyright (c) 2008, Cillian de Róiste
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import BeautifulSoup
import urllib
import urllib2
import re

class Feg(callbacks.Plugin):
    """Add the help for "@plugin help Feg" here
    This should describe *how* to use this plugin."""
    threaded = True
    extragubbins = ["\n","\r", "&nbsp;","a1","a2","a3","a4","a5","intr","fir1","fir2","fir3","fir4","fir5","bain1","bain2","bain3","bain4", "bain5", "s","br"]

    def abair(self, irc, msg, args, query):
        url = "http://www.abair.tcd.ie/index.php?view=files&"
        query = urllib.urlencode({'input': query})
        response = urllib2.urlopen(url + query)
        the_page = response.read()
        soup = BeautifulSoup.BeautifulSoup(the_page)
        mp3 = soup.findAll("div", {"class": "view"})[0].findAll("a")[0]['href']
        irc.reply(mp3)
    abair = wrap(abair, ['text'])

    def focail(self, lang, query):
        """
        helper method to do the work
        """
        res = ""
        url = "http://www.tearma.ie/Search.aspx?term=%s&lang=%s" % (query,lang) 
        foc = urllib2.urlopen(url).readlines()
        foc_soup = BeautifulSoup.BeautifulSoup("".join(foc))
        for entry in foc_soup.findAll("div", {"class":"dTerm"}):
            for count, item in enumerate(entry.findAll("span", {"class":"dWording"})):
                for text in item.findAll(text=True):
                    if text not in self.extragubbins:
                        res += text
                if count % 2 == 1:
                    res += ";"
                else:
                    res += " :"
                res += " "
        if not res:
            res = u"Níl a fhios ag tearma.ie ar chor ar bith :/ Bain trial as %s" %url.decode("utf-8")
        return res

    def feg(self, irc, msg, args, query):
        """<ceist> 
        
        cuardaigh ar tearma.ie: English->Gaeilge"""
        try:
            res = self.focail(1,query)
            res = re.sub('[\r\t\n]', ' ', res)
            res = re.sub(' +', ' ', res)
            irc.reply(res.encode('utf8'))
        except Exception, message:
            irc.reply("Dar le feg: %s" %message)
    feg = wrap(feg, ['text'])

    def fge(self, irc, msg, args, query):
        """<ceist> 
        
        cuardaigh ar tearma.ie: Gaeilge->English"""
        try:
            res = self.focail(2,query)        
            res = re.sub('[\r\t\n]', ' ', res)
            res = re.sub(' +', ' ', res)
            #irc.reply(res.encode('utf8'))
            irc.reply(res)
        except Exception, message:
            irc.reply("Dar le fge: %s" %message)
    fge = wrap(fge, ['text'])

Class = Feg


# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
